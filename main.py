import random
import pygame
import Field
import Ghost
import Pacman
import Blinky
import Pinky
import Inky
import Clyde
import game_func
import Button
import Score
from constants_and_variables import bool_for_game, volume, music_arr, main_color
from handle_events import handle_menu_events, handle_events_for_game, handle_over_events

screen = None
size = width, height = 448, 576
caption = 'Pac-Man'
pygame.font.init()
font = pygame.font.SysFont('Comic Sans MS', 40)
but_font = pygame.font.SysFont('Comic Sans MS', 30)
logo = pygame.image.load('pac_logo1.png')
logo = pygame.transform.scale(logo, (400, 200))
score_font = pygame.font.SysFont('Comic Sans MS', 20)


def menu():
    global logo
    start = Button.Button('Start', (0, 0, 0), (255, 255, 0), but_font)
    settings = Button.Button('Settings', (0, 0, 0), (255, 255, 0), but_font)
    score = Button.Button('Score', (0, 0, 0), (255, 255, 0), but_font)
    exit = Button.Button('Exit', (0, 0, 0), (255, 255, 0), but_font)

    blinky = Blinky.Blinky(15, 2, 20, 225, 105)
    blinky.direction = 'w'
    pinky = Pinky.Pinky(15, 2, 16, 128, 65)
    inky = Inky.Inky(15, 2, 13, 230, 172)
    inky.direction = 's'
    clyde = Clyde.Clyde(15, 2, 14, 405, 125)
    clyde.direction = 'd'

    while bool_for_game['menu']:
        screen.fill(main_color)
        screen.blit(logo, (35, 25))
        pinky.draw(screen)
        inky.draw(screen)
        blinky.draw(screen)
        clyde.draw(screen)

        start.show(screen, width // 2 - start.width // 2, 260)
        start.check_mouse()
        settings.show(screen, width // 2 - settings.width // 2, start.y + start.height + 15)
        settings.check_mouse()
        score.show(screen, width // 2 - score.width // 2, settings.y + settings.height + 15)
        score.check_mouse()
        exit.show(screen, width // 2 - exit.width // 2, score.y + score.height + 15)
        exit.check_mouse()

        handle_menu_events(screen, start, settings, score, exit)
        pygame.time.delay(15)
        pygame.display.update()


def game():
    heart = pygame.transform.scale(pygame.image.load('pixel-heart.png'), (16, 16)).convert()
    field = Field.Field(width)
    Ghost.timer = 0
    pacman = Pacman.Pacman(16, 2, 15, field.door[0] + 17, field.door[1] + 5 * 16 + 8)
    blinky = Blinky.Blinky(15, 2, 16, field.door[0] + 17, field.door[1] - 8)
    pinky = Pinky.Pinky(15, 2, 16, field.door[0] + 17, field.door[1] + 2 * 16 + 8)
    inky = Inky.Inky(15, 2, 16, field.door[0] - 16, field.door[1] + 2 * 16 + 8)
    clyde = Clyde.Clyde(15, 2, 16, field.door[0] + 16 * 3, field.door[1] + 2 * 16 + 8)
    ghosts_arr = [blinky, pinky, inky, clyde]
    score = Score.Score(score_font)
    with open("high_score.txt", "r") as f:
        high_score = f.read().split('\n')[0]
    high_score = score_font.render('high score: ' + high_score, False, (255, 255, 255))
    game_func.show_start(1800, screen, field, blinky, pinky, inky, clyde, pacman, score, heart, high_score)
    while bool_for_game['game']:
        screen.fill((0, 0, 0))
        field.draw(screen)
        pacman.draw(screen)
        pacman.move(field, [blinky, pinky, inky, clyde])
        blinky.draw(screen)
        blinky.move_blinky(field, pacman, pinky, inky, clyde, screen, score, heart, high_score)
        pinky.draw(screen)
        pinky.move_pinky(field, pacman, blinky, inky, clyde, screen, score, heart, high_score)
        inky.draw(screen)
        inky.move_inky(field, pacman, blinky, pinky, clyde, screen, score, heart, high_score)
        clyde.draw(screen)
        clyde.move_clyde(field, pacman, blinky, pinky, inky, screen, score, heart, high_score)
        score.show(screen, 70, 10, (255, 255, 255))
        screen.blit(high_score, (230, 10))
        for i in range(pacman.life):
            screen.blit(heart, (50 + 24 * i, 35 * 16 - 8))
        if pacman.death:
            bool_for_game['game'] = False
            bool_for_game['over'] = True
            game_func.write_score(Ghost.Ghost.count)
        if Field.Field.grains == 0:
            bool_for_game['game'] = False
            bool_for_game['win'] = True
            game_func.write_score(Ghost.Ghost.count)

        handle_events_for_game(screen, field, pacman, score, ghosts_arr)
        pygame.time.delay(15)
        pygame.display.update()


def over():
    coo = []
    wid = 0
    text = [font.render(i, False, (200, 200, 0)) for i in 'Game Over'] if bool_for_game['over'] else [font.render(i, False, (200, 200, 0)) for i in 'Win']
    pacman = Pacman.Pacman(32, 2, 15, 20, 200)
    pacman.direction = 'd'
    restart = Button.Button('restart', (0, 0, 0), (255, 255, 0), but_font)
    main_menu = Button.Button('main menu', (0, 0, 0), (255, 255, 0), but_font)
    surf = score_font.render('score: ' + str(Ghost.Ghost.count), False, (0, 0, 0))
    for i in range(len(text)):
        coo.append(448 + wid)
        wid += text[i].get_width()
    while bool_for_game['over'] or bool_for_game['win']:
        screen.fill(main_color)
        for i in range(len(text)):
            screen.blit(text[i], (coo[i], 167))
            coo[i] -= 2
            if coo[i] < 43:
                coo[i] = 448
        pacman.draw(screen)
        screen.blit(surf, (width // 2 - surf.get_width() // 2, 180 - 30 - surf.get_height()))
        restart.show(screen, width // 2 - restart.width // 2, 180 + text[0].get_height() + 60)
        main_menu.show(screen, width // 2 - main_menu.width // 2, 180 + text[0].get_height() + 80 + restart.height)

        handle_over_events(restart, main_menu)
        pygame.time.delay(15)
        pygame.display.update()


def init():
    global caption, screen
    pygame.init()
    pygame.display.set_caption(caption)
    screen = pygame.display.set_mode(size)
    pygame.display.set_icon(pygame.image.load('main_icon.png'))


init()
pygame.mixer.music.load('music/' + random.choice(music_arr))
pygame.mixer.music.play()
pygame.mixer.music.set_volume(volume)
pygame.mixer.music.set_endevent(pygame.USEREVENT + 1)

while 1 == 1:
    Ghost.Ghost.count = 0
    menu()
    game()
    over()
