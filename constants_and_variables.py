bool_for_game = {'menu': True, 'game': False, 'over': False, 'win': False}

music_arr = ['Smells Like Teen Spirit.mp3', 'Billie Jean.mp3', 'Bohemian Rhapsody.mp3', 'Centuries.mp3',
             'Come Together.mp3', 'Hotel California.mp3', 'Last Christmas.mp3', 'Let It Be.mp3',
             'Never Gonna Give You Up.mp3', 'Penny Lane.mp3', 'Take Me To Church.mp3',
             'Viva La Vida.mp3', 'Wake Me Up.mp3', 'Demons.mp3', 'Fly Me To The Moon.mp3', 'I Bet My Life.mp3',
             'In The End.mp3', 'MSKWYDITD.mp3', 'Numb.mp3', 'People Are Strange.mp3', 'Radioactive.mp3',
             'Seven Nation Army.mp3', 'Take On Me.mp3', 'Warriors.mp3']

volume = 1
time_with_pauses = 0
map = 'original'
main_color = (66, 94, 104)