import Ghost
import pygame


class Inky(Ghost.Ghost):
    def __init__(self, size, speed, delay, x, y):
        Ghost.Ghost.__init__(self, size, speed, delay, x, y)
        self.count = 300
        self.direction = 'd'
        self.image = ['Ghosts-sprites/Inky/Inky_up_0.png', 'Ghosts-sprites/Inky/Inky_up_1.png',
                      'Ghosts-sprites/Inky/Inky_left_0.png', 'Ghosts-sprites/Inky/Inky_left_1.png',
                      'Ghosts-sprites/Inky/Inky_down_0.png', 'Ghosts-sprites/Inky/Inky_down_1.png',
                      'Ghosts-sprites/Inky/Inky_right_0.png', 'Ghosts-sprites/Inky/Inky_right_1.png']
        for i in range(8):
            self.image[i] = pygame.transform.scale(pygame.image.load(self.image[i]), (self.size, self.size)).convert()
            self.image[i].set_colorkey((0, 0, 0))

    def move_inky(self, field, pacman, blinky, pinky, clyde, screen, score, heart, high_score):
        if Ghost.Ghost.count < self.count:
            return

        # ------------target------------------
        if self.alive and self.out:
            if Ghost.Ghost.mode == 'chase':
                if pacman.direction == 'a':
                    self.target = (blinky.x - 2 * (blinky.x - (pacman.x - 16 * 2)), blinky.y - 2 * (blinky.y - pacman.y))
                elif pacman.direction == 'd':
                    self.target = (blinky.x - 2 * (blinky.x - (pacman.x + 16 * 2)), blinky.y - 2 * (blinky.y - pacman.y))
                elif pacman.direction == 'w':
                    self.target = (blinky.x - 2 * (blinky.x - pacman.x), blinky.y - 2 * (blinky.y - (pacman.y - 16 * 2)))
                elif pacman.direction == 's':
                    self.target = (blinky.x - 2 * (blinky.x - pacman.x), blinky.y - 2 * (blinky.y - (pacman.y + 16 * 2)))
                elif pacman.direction == '':
                    self.target = (blinky.x - 2 * (blinky.x - pacman.x), blinky.y - 2 * (blinky.y - pacman.y))
                if self.target[0] < 0:
                    y = self.target[1]
                    self.target = (0, y)
                if self.target[1] < 0:
                    x = self.target[0]
                    self.target = (x, 0)

            elif Ghost.Ghost.mode == 'scatter':
                self.target = (448, 576)

        self.check_pacman_collision(pacman, [blinky, pinky, self, clyde], screen, field, score, heart, high_score)

        # for i in range(28):
        #     for j in range(36):
        #         field.arr[i][j].target = False
        # try:
        #     field.arr[self.target[0] // 16][self.target[1] // 16].target = True
        # except IndexError:
        #     print(self.target)

        Ghost.Ghost.move(self, screen, field)
