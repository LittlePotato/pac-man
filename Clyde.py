import Ghost
import pygame


class Clyde(Ghost.Ghost):
    def __init__(self, size, speed, delay, x, y):
        Ghost.Ghost.__init__(self, size, speed, delay, x, y)
        self.direction = 'a'
        self.count = 600
        self.image = ['Ghosts-sprites/Clyde/Clyde_up_0.png', 'Ghosts-sprites/Clyde/Clyde_up_1.png',
                      'Ghosts-sprites/Clyde/Clyde_left_0.png', 'Ghosts-sprites/Clyde/Clyde_left_1.png',
                      'Ghosts-sprites/Clyde/Clyde_down_0.png', 'Ghosts-sprites/Clyde/Clyde_down_1.png',
                      'Ghosts-sprites/Clyde/Clyde_right_0.png', 'Ghosts-sprites/Clyde/Clyde_right_1.png']
        for i in range(8):
            self.image[i] = pygame.transform.scale(pygame.image.load(self.image[i]), (self.size, self.size)).convert()
            self.image[i].set_colorkey((0, 0, 0))

    def move_clyde(self, field, pacman, blinky, pinky, inky, screen, score, heart, high_score):
        if Ghost.Ghost.count < self.count:
            return

        # ------------target------------------
        if self.alive and self.out:
            if Ghost.Ghost.mode == 'chase' and abs(pacman.x - self.x) > 8 and abs(pacman.y - self.y) > 8:
                self.target = (pacman.x, pacman.y)
            elif Ghost.Ghost.mode == 'chase':
                self.target = (0, 576)
            elif Ghost.Ghost.mode == 'scatter':
                self.target = (0, 576)

        self.check_pacman_collision(pacman, [blinky, pinky, inky, self], screen, field, score, heart, high_score)

        Ghost.Ghost.move(self, screen, field)
