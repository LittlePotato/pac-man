import Ghost
import pygame


class Blinky(Ghost.Ghost):
    def __init__(self, size, speed, delay, x, y):
        Ghost.Ghost.__init__(self, size, speed, delay, x, y)
        self.out = True
        self.in_center = True
        self.direction = 'a'
        self.image = ['Ghosts-sprites/Blinky/Blinky_up_0.png', 'Ghosts-sprites/Blinky/Blinky_up_1.png',
                      'Ghosts-sprites/Blinky/Blinky_left_0.png', 'Ghosts-sprites/Blinky/Blinky_left_1.png',
                      'Ghosts-sprites/Blinky/Blinky_down_0.png', 'Ghosts-sprites/Blinky/Blinky_down_1.png',
                      'Ghosts-sprites/Blinky/Blinky_right_0.png', 'Ghosts-sprites/Blinky/Blinky_right_1.png']
        for i in range(8):
            self.image[i] = pygame.transform.scale(pygame.image.load(self.image[i]), (self.size, self.size)).convert()
            self.image[i].set_colorkey((0, 0, 0))

    def move_blinky(self, field, pacman, pinky, inky, clyde, screen, score, heart, high_score):
        # ------------target------------------
        if self.alive and self.out:
            if Ghost.Ghost.mode == 'chase':
                self.target = (pacman.x, pacman.y)
            else:
                self.target = (448, 0)

        self.check_pacman_collision(pacman, [self, pinky, inky, clyde], screen, field, score, heart, high_score)

        Ghost.Ghost.move(self, screen, field)
