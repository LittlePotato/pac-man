import pygame


class RadioButton:
    def __init__(self, num_of_parts, color_off, color_on, font, image=None, width_of_button=20,
                 space_between_parts=20, space_between_button_and_text=10, text_arr=None, orientation='vertical',
                 space_in_h=10):
        self.num_of_parts = num_of_parts
        self.orientation = orientation                                              # vertical, horizontal, special(coordinates)
        self.text_arr = text_arr if text_arr is not None else [str(i + 1) for i in range(self.num_of_parts)]
        self.color_off = color_off
        self.color_on = color_on
        self.cur_color = [color_off for i in range(self.num_of_parts)]
        self.disabled_color = ((self.color_off[0] + 255) // 2, (self.color_off[1] + 255) // 2,
                               (self.color_off[2] + 255) // 2)
        self.disabled = False
        self.state = [False for i in range(self.num_of_parts)]
        self.space_between_button_and_text = space_between_button_and_text
        self.space_between_parts = space_between_parts
        self.font = font
        self.width_of_button = width_of_button
        self.text_wid_arr = []
        self.text_hei = 0
        self.space_in_h = space_in_h
        self.full_width = 0
        self.full_height = 0
        self.set_full_width_height()
        self.image = image
        if self.image is not None:
            self.image = pygame.transform.scale(self.image, (self.width_of_button // 2, self.width_of_button // 2))
        self.x = 0
        self.y = 0

    def set_full_width_height(self):
        w = (self.width_of_button + self.space_between_button_and_text) * self.num_of_parts + \
            self.space_between_parts * (self.num_of_parts - 1)
        max_w = 0
        max_h = 0
        h = self.space_in_h * (self.num_of_parts - 1)
        for i in range(self.num_of_parts):
            textsurf = self.font.render(self.text_arr[i], False, self.cur_color[i])
            w += textsurf.get_width()
            h += textsurf.get_height()
            if textsurf.get_width() + self.width_of_button + self.space_between_button_and_text > max_w:
                max_w = textsurf.get_width() + self.width_of_button + self.space_between_button_and_text
                max_h = textsurf.get_height()
            self.text_hei = textsurf.get_height()
            self.text_wid_arr.append(textsurf.get_width())
        self.full_width = w if self.orientation == 'horizontal' else max_w
        self.full_height = h if self.orientation == 'vertical' else max_h

    def show(self, screen, x, y):
        self.x = x
        self.y = y
        if self.orientation == 'vertical' or self.orientation == 'horizontal':
            x_cur = x
            y_cur = y
        else:
            x_cur = x[0]
            y_cur = y[0]
        for i in range(self.num_of_parts):
            cent = (x_cur + self.width_of_button // 2, y_cur + self.width_of_button // 2)
            color = self.cur_color[i] if not self.disabled else self.disabled_color
            circ_color = self.color_off if not self.disabled else self.disabled_color
            pygame.draw.circle(screen, circ_color, cent, self.width_of_button // 2, 2)
            if self.state[i] and self.image is None:
                pygame.draw.circle(screen, circ_color, cent, self.width_of_button // 4, 0)
            elif self.state[i]:
                screen.blit(self.image, (cent[0] - self.width_of_button // 4, cent[1] - self.width_of_button // 4))
                if self.disabled:
                    # pygame.draw.circle(screen, (255, 255, 255, 200), cent, self.width_of_button // 2, 0)
                    pass
            textsurf = self.font.render(self.text_arr[i], False, color)
            screen.blit(textsurf, (x_cur + self.width_of_button + self.space_between_button_and_text, y_cur))
            if self.orientation == 'horizontal':
                x_cur += self.width_of_button + self.space_between_button_and_text + textsurf.get_width() + \
                        self.space_between_parts
            elif self.orientation == 'vertical':
                y_cur += self.text_hei + self.space_in_h
            else:
                x_cur = x[i + 1] if i != self.num_of_parts - 1 else x_cur
                y_cur = y[i + 1] if i != self.num_of_parts - 1 else y_cur

    def check_click(self):
        pos = pygame.mouse.get_pos()
        if self.orientation == 'vertical' or self.orientation == 'horizontal':
            cur_x = self.x
            cur_y = self.y
        else:
            cur_x = self.x[0]
            cur_y = self.y[0]
        new_state = []
        for i in range(self.num_of_parts):
            cur_wid = self.width_of_button + self.space_between_button_and_text + self.text_wid_arr[i]
            if cur_x <= pos[0] <= cur_x + cur_wid and cur_y <= pos[1] <= cur_y + self.text_hei and not self.disabled:
                new_state.append(True)
            elif not self.disabled:
                new_state.append(False)
            if self.orientation == 'vertical':
                cur_y = cur_y + self.text_hei + self.space_in_h
            elif self.orientation == 'horizontal':
                cur_x = cur_x + cur_wid + self.space_between_parts
            elif i != self.num_of_parts - 1:
                cur_x = self.x[i + 1]
                cur_y = self.y[i + 1]
        if True in new_state:
            self.state = new_state

    def check_mouse(self):
        pos = pygame.mouse.get_pos()
        if self.orientation == 'vertical' or self.orientation == 'horizontal':
            x_cur = self.x
            y_cur = self.y
        else:
            x_cur = self.x[0]
            y_cur = self.y[0]
        for i in range(self.num_of_parts):
            cur_wid = self.width_of_button + self.space_between_button_and_text + self.text_wid_arr[i]
            if x_cur <= pos[0] <= x_cur + cur_wid and y_cur <= pos[1] <= y_cur + self.text_hei:
                self.cur_color[i] = self.color_on
            else:
                self.cur_color[i] = self.color_off
            if self.orientation == 'vertical':
                y_cur = y_cur + self.text_hei + self.space_in_h
            elif self.orientation == 'horizontal':
                x_cur = x_cur + cur_wid + self.space_between_parts
            elif i != self.num_of_parts - 1:
                x_cur = self.x[i + 1]
                y_cur = self.y[i + 1]
