import pygame
on_im = pygame.image.load('icons/checkbox.png')
off_im = pygame.image.load('icons/empty_checkbox.png')


class CheckBox:
    def __init__(self, color_off, color_on, font, image_on=on_im, image_off=off_im, state=False, space=10, text_on='on',
                 text_off='off', w=15):
        self.x = 0
        self.y = 0
        self.width = w
        self.state = state
        self.color_on = color_on
        self.color_off = color_off
        self.cur_color = color_off
        self.image_on = pygame.transform.scale(image_on, (w, w))
        self.image_off = pygame.transform.scale(image_off, (w, w))
        self.text_on = text_on
        self.text_off = text_off
        self.font = font
        self.space_between_box_and_text = space
        self.textsurface_on = self.font.render(self.text_on, False, self.cur_color)
        self.textsurface_off = self.font.render(self.text_off, False, self.cur_color)
        self.cur_text_width = 0
        self.text_on_width = self.textsurface_on.get_width()
        self.text_off_width = self.textsurface_off.get_width()
        self.disabled = False
        self.disabled_color = ((self.color_off[0] + 255) // 2, (self.color_off[1] + 255) // 2,
                               (self.color_off[2] + 255) // 2)

    def show(self, screen, x, y):
        self.x = x
        self.y = y
        if not self.disabled:
            if self.state:
                screen.blit(self.image_on, (self.x, self.y))
                textsurface = self.font.render(self.text_on, False, self.cur_color)
                self.cur_text_width = self.text_on_width
            else:
                screen.blit(self.image_off, (self.x, self.y))
                textsurface = self.font.render(self.text_off, False, self.cur_color)
                self.cur_text_width = self.text_off_width
        else:
            if self.state:
                screen.blit(self.image_on, (self.x, self.y))
                textsurface = self.font.render(self.text_on, False, self.disabled_color)
                self.cur_text_width = self.text_on_width
            else:
                screen.blit(self.image_off, (self.x, self.y))
                textsurface = self.font.render(self.text_off, False, self.disabled_color)
                self.cur_text_width = self.text_off_width

        screen.blit(textsurface, (self.x + self.width + self.space_between_box_and_text, self.y))

        if self.disabled:
            dis_surf = pygame.Surface((self.width, self.width), pygame.SRCALPHA)
            dis_surf.fill((255, 255, 255, 100))
            screen.blit(dis_surf, (self.x, self.y))

    def check_click(self):
        pos = pygame.mouse.get_pos()
        if self.x <= pos[0] <= self.x + self.width + self.space_between_box_and_text + self.cur_text_width \
                and self.y <= pos[1] <= self.y + self.width and not self.disabled:
            self.state = not self.state

    def check_mouse(self):
        pos = pygame.mouse.get_pos()
        if self.x <= pos[0] <= self.x + self.width + self.space_between_box_and_text + self.cur_text_width \
                and self.y <= pos[1] <= self.y + self.width and not self.disabled:
            self.cur_color = self.color_on
        else:
            self.cur_color = self.color_off

