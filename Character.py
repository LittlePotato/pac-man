class Character:
    def __init__(self, size, speed, delay, x, y):
        self.size = size
        self.speed = speed
        self.delay = delay
        self.x = x
        self.y = y
        self.start_x = x
        self.start_y = y
        self.default_speed = speed
        self.time = 0
        self.cur = 0
        self.dict = {'a': 'left', 'd': 'right', 'w': 'up', 's': 'down'}
        self.cache = None
        self.cache_timer = 0

    def move(self, screen, field, ghosts):
        row_cell, column_cell = int(self.y // field.arr[0][0].width), int(self.x // field.arr[0][0].width)

        # ----------------tunnel---------------
        if self.direction == 'a' and (self.x - self.speed) // field.arr[0][0].width < 0:
            self.x = field.arr[0][27].coo[0] + field.arr[0][0].width
            column_cell = 27
        elif self.direction == 'd' and (self.x + self.speed) // field.arr[0][0].width > 27:
            self.x = field.arr[0][0].coo[0]
            column_cell = 0

        # -------------------------------------
        if self.direction == 'd':
            self.x += self.speed
        elif self.direction == 'a':
            self.x -= self.speed
        elif self.direction == 'w':
            self.y -= self.speed
        elif self.direction == 's':
            self.y += self.speed

        self.cache_timer -= 1  # cache is not endless
        # -----------put character in the center of a cell------------
        if self.direction == 'd' and self.x < field.arr[row_cell][column_cell].coo[0] + field.arr[row_cell][column_cell].width // 2 \
                <= self.x + self.speed:
            self.x = field.arr[row_cell][column_cell].coo[0] + field.arr[row_cell][column_cell].width // 2

        elif self.direction == 'a' and self.x > field.arr[row_cell][column_cell].coo[0] + field.arr[row_cell][column_cell].width // 2 \
                >= self.x - self.speed:
            self.x = field.arr[row_cell][column_cell].coo[0] + field.arr[row_cell][column_cell].width // 2

        elif self.direction == 'w' and self.y > field.arr[row_cell][column_cell].coo[1] + field.arr[row_cell][column_cell].width // 2 \
                >= self.y - self.speed:
            self.y = field.arr[row_cell][column_cell].coo[1] + field.arr[row_cell][column_cell].width // 2

        elif self.direction == 's' and self.y < field.arr[row_cell][column_cell].coo[1] + field.arr[row_cell][column_cell].width // 2 \
                <= self.y + self.speed:
            self.y = field.arr[row_cell][column_cell].coo[1] + field.arr[row_cell][column_cell].width // 2
        elif self.direction != '':
            return

        # ----------------turn---------------------
        if self.cache is not None and self.cache_timer > 0 and self.dict[self.cache] in field.arr[row_cell][column_cell].ways:
            self.direction = self.cache
            self.cache = None
            if self.direction == 'd':
                self.x += self.speed
            elif self.direction == 'a':
                self.x -= self.speed
            elif self.direction == 'w':
                self.y -= self.speed
            elif self.direction == 's':
                self.y += self.speed

        elif self.direction != '' and not self.dict[self.direction] in field.arr[row_cell][column_cell].ways and column_cell != 0 and column_cell != 27:
            self.direction = ''
