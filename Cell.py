from math import radians

import pygame


class Cell:
    color = (0, 0, 255)
    door_color = (123, 61, 25)

    def __init__(self, state, width, coo):
        self.state = state     # 0 - empty, 1 - wall, 2 - grain, 3 - big grain
        self.width = width
        self.coo = coo
        self.ways = []
        self.double_wall = False
        self.door = False

    def draw(self, screen, field, i, j):
        # pygame.draw.rect(screen, (50, 50, 50), (self.coo[0], self.coo[1], self.width, self.width), 1)
        if self.state == 1:

            if self.double_wall:
                pygame.draw.rect(screen, (5, 10, 65), (self.coo[0], self.coo[1], self.width, self.width), 0)

            # -----------------------------------------------------------------------------
            elif self.door:
                if i != 0 and field.arr[i - 1][j].state == 1 or i != 35 and field.arr[i + 1][j].state == 1:   # vertical
                    pygame.draw.line(screen, Cell.door_color, (self.coo[0] + self.width // 2, self.coo[1]),
                                     (self.coo[0] + self.width // 2, self.coo[1] + self.width), 3)
                else:                                                                                       # horizontal
                    pygame.draw.line(screen, Cell.door_color, (self.coo[0], self.coo[1] + self.width // 2),
                                     (self.coo[0] + self.width, self.coo[1] + self.width // 2), 3)

            # -----------------------------------------------------------------------------
            else:
                if i != 0 and field.arr[i - 1][j].state != 1:        # сверху коридор
                    if j != 27 and field.arr[i][j + 1].state != 1:    # сверху справа
                        pygame.draw.arc(screen, Cell.color, (self.coo[0] - self.width // 2, self.coo[1] + self.width // 2, self.width + 2, self.width + 2),
                                        radians(0), radians(90), 2)
                    elif j != 0 and field.arr[i][j - 1].state != 1:      # сверху слева
                        pygame.draw.arc(screen, Cell.color, (self.coo[0] + self.width // 2, self.coo[1] + self.width // 2, self.width + 2, self.width + 2),
                                        radians(90), radians(180), 2)
                    else:
                        pygame.draw.line(screen, Cell.color,
                                         (self.coo[0], self.coo[1] + self.width // 2),
                                         (self.coo[0] + self.width, self.coo[1] + self.width // 2), 2)

                elif i != 35 and field.arr[i + 1][j].state != 1:        # снизу
                    if j != 27 and field.arr[i][j + 1].state != 1:    # снизу справа
                        pygame.draw.arc(screen, Cell.color, (self.coo[0] - self.width // 2, self.coo[1] - self.width // 2, self.width + 2, self.width + 2),
                                        radians(270), radians(360), 2)
                    elif j != 0 and field.arr[i][j - 1].state != 1:      # снизу слева
                        pygame.draw.arc(screen, Cell.color, (self.coo[0] + self.width // 2, self.coo[1] - self.width // 2, self.width + 2, self.width + 2),
                                        radians(180), radians(270), 2)
                    else:
                        pygame.draw.line(screen, Cell.color,
                                         (self.coo[0], self.coo[1] + self.width // 2),
                                         (self.coo[0] + self.width, self.coo[1] + self.width // 2), 2)
                elif j != 0 and field.arr[i][j - 1].state != 1 or j != 27 and field.arr[i][j + 1].state != 1:  # вертикаль
                    pygame.draw.line(screen, Cell.color, (self.coo[0] + self.width // 2, self.coo[1]),
                                     (self.coo[0] + self.width // 2, self.coo[1] + self.width), 2)
                elif j != 27:                                              # наискосок
                    if field.arr[i - 1][j - 1].state != 1:
                        pygame.draw.arc(screen, Cell.color, (self.coo[0] - self.width // 2, self.coo[1] - self.width // 2, self.width + 2, self.width + 2),
                                        radians(270), radians(360), 2)
                    elif field.arr[i - 1][j + 1].state != 1:
                        pygame.draw.arc(screen, Cell.color, (self.coo[0] + self.width // 2, self.coo[1] - self.width // 2, self.width + 2, self.width + 2),
                                        radians(180), radians(270), 2)
                    elif field.arr[i + 1][j - 1].state != 1:
                        pygame.draw.arc(screen, Cell.color, (self.coo[0] - self.width // 2, self.coo[1] + self.width // 2, self.width + 2, self.width + 2),
                                        radians(0), radians(90), 2)
                    else:
                        pygame.draw.arc(screen, Cell.color, (self.coo[0] + self.width // 2, self.coo[1] + self.width // 2, self.width + 2, self.width + 2),
                                        radians(90), radians(180), 2)

        elif self.state == 2:
            pygame.draw.circle(screen, (255, 153, 114), (self.coo[0] + self.width // 2, self.coo[1] + self.width // 2),
                               self.width // 6, 0)
        elif self.state == 3:
            pygame.draw.circle(screen, (255, 153, 114), (self.coo[0] + self.width // 2, self.coo[1] + self.width // 2),
                               self.width // 2 - 2, 0)
