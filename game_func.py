import random
import sys
import pygame
from constants_and_variables import music_arr
import constants_and_variables
from RadioButton import RadioButton
from Button import Button
from Slider import Slider


pygame.font.init()
font = pygame.font.SysFont('Comic Sans MS', 25)
big_font = pygame.font.SysFont('Comic Sans MS', 40)
size = width, height = 448, 576
volume = 1
slider_font = pygame.font.SysFont('Comic Sans MS', 15)


def handle_animation_function_events(screen, score):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE or event.key == pygame.K_p:
                pause(screen, score)
        elif event.type == pygame.mixer.music.get_endevent():
            pygame.mixer.music.load('music/' + random.choice(music_arr))
            pygame.mixer.music.play()
            pygame.mixer.music.set_volume(volume)


def handle_pause_events(back, menu):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.mixer.music.get_endevent():
            pygame.mixer.music.load('music/' + random.choice(music_arr))
            pygame.mixer.music.play()
            pygame.mixer.music.set_volume(volume)
        if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
            return False
        if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            if back.check_click():
                return False
            if menu.check_click():
                constants_and_variables.bool_for_game['game'] = False
                constants_and_variables.bool_for_game['over'] = False
                constants_and_variables.bool_for_game['win'] = False
                constants_and_variables.bool_for_game['menu'] = True
                return False
    return True


def show_start(time, screen, field, blinky, pinky, inky, clyde, pacman, score, heart, high_score):     # таймер игры обнуляется
    timer = 0
    blinky.x, blinky.y, blinky.direction, blinky.alive, blinky.out, blinky.speed = blinky.start_x, blinky.start_y, 'a', True, True, 2
    pinky.x, pinky.y, pinky.direction, pinky.alive, pinky.out, pinky.speed, pinky.in_center = pinky.start_x, pinky.start_y, 'a', True, False, 2, False
    inky.x, inky.y, inky.direction, inky.alive, inky.out, inky.speed, inky.in_center = inky.start_x, inky.start_y, 'd', True, False, 2, False
    clyde.x, clyde.y, clyde.direction, clyde.alive, clyde.out, clyde.speed, clyde.in_center = clyde.start_x, clyde.start_y, 'a', True, False, 2, False
    pacman.x, pacman.y, pacman.direction = pacman.start_x, pacman.start_y, 'a'
    ready = font.render('READY', False, (255, 255, 0))
    while 1 == 1:

        if timer >= time:
            break
        timer += 15

        screen.fill((0, 0, 0))
        field.draw(screen)
        blinky.draw(screen)
        pinky.draw(screen)
        inky.draw(screen)
        clyde.draw(screen)
        score.show(screen, 70, 10, (255, 255, 255))
        screen.blit(high_score, (230, 10))
        pacman.draw(screen)
        for i in range(pacman.life):
            screen.blit(heart, (50 + 24 * i, 35 * 16 - 8))
        if timer % 500 <= 250:
            screen.blit(ready, (screen.get_width() // 2 - ready.get_width() // 2, 31 * 16))

        handle_animation_function_events(screen, score)
        pygame.time.delay(15)
        pygame.display.update()


def show_pacman_death(screen, field, blinky, pinky, inky, clyde, pacman, score):     # таймер игры останавливается
    timer = 0
    death_arr = ['pac-man-sprites/Pac_death_0.png', 'pac-man-sprites/Pac_death_1.png',
                 'pac-man-sprites/Pac_death_2.png',
                 'pac-man-sprites/Pac_death_3.png', 'pac-man-sprites/Pac_death_4.png',
                 'pac-man-sprites/Pac_death_5.png',
                 'pac-man-sprites/Pac_death_6.png', 'pac-man-sprites/Pac_death_7.png',
                 'pac-man-sprites/Pac_death_8.png',
                 'pac-man-sprites/Pac_death_9.png', 'pac-man-sprites/Pac_death_10.png']

    for i in range(11):
        death_arr[i] = pygame.transform.scale(pygame.image.load(death_arr[i]), (16, 16))
    blinky.auto = True
    pinky.auto = True
    inky.auto = True
    clyde.auto = True

    while 1 == 1:
        screen.fill((0, 0, 0))
        field.draw(screen)
        pacman.draw(screen)
        blinky.draw(screen)
        pinky.draw(screen)
        inky.draw(screen)
        clyde.draw(screen)

        if timer >= 300:
            break
        timer += 15

        handle_animation_function_events(screen, score)
        pygame.time.delay(15)
        pygame.display.update()

    timer = 0
    cur_plus = 0
    cur = 0
    while 1 == 1:
        if timer >= 820:
            break
        if cur_plus >= 70:
            cur_plus = 0
            if cur != 10:
                cur += 1

        screen.fill((0, 0, 0))
        field.draw(screen)
        screen.blit(death_arr[cur], (pacman.x - 8, pacman.y - 8))

        cur_plus += 15
        timer += 15

        handle_animation_function_events(screen, score)
        pygame.time.delay(15)
        pygame.display.update()


def pause(screen, score):
    now_pause = True
    back = Button('back', (0, 0, 0), (255, 255, 0), font)
    main_menu = Button('main menu', (0, 0, 0), (255, 255, 0), font)
    pause_label = big_font.render('PAUSE', False, (0, 0, 0))

    while now_pause:
        screen.fill(constants_and_variables.main_color)
        score.show(screen, 200, 10, (255, 255, 255))
        main_menu.show(screen, 170, 250)
        back.show(screen, 200, 300)
        screen.blit(pause_label, (170, 100))

        now_pause = handle_pause_events(back, main_menu)
        pygame.time.delay(15)
        pygame.display.update()


def handle_settings_events(map, slider, back):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.mixer.music.get_endevent():
            pygame.mixer.music.load('music/' + random.choice(music_arr))
            pygame.mixer.music.play()
            pygame.mixer.music.set_volume(volume)
        if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            return False
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            slider.check_mouse_click()
        if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            slider.unclick()
            map.check_click()
            n = 0
            for i in map.state:
                if i is True:
                    n = i
            constants_and_variables.map = map.text_arr[n]
            if back.check_click():
                return False

    return True


def settings(screen):
    now_set = True
    map = RadioButton(2, (0, 0, 0), (255, 255, 0), font, text_arr=['original', 'special'])
    map.state = [True, False] if constants_and_variables.map == 'original' else [False, True]
    map_label = font.render('field: ', False, (0, 0, 0))
    slider = Slider(constants_and_variables.volume, 15, color=(255, 255, 0), font=slider_font)
    slider.color_right = (86, 114, 124)
    back = Button('back', (0, 0, 0), (255, 255, 0), font)

    while now_set:
        screen.fill(constants_and_variables.main_color)
        map.show(screen, 200, 70)
        screen.blit(map_label, (30, 70))
        map.check_mouse()
        slider.show(screen, 70, 200)
        slider.check_mouse_move()
        back.show(screen, 200, 500)

        now_set = handle_settings_events(map, slider, back)
        constants_and_variables.volume = slider.volume
        pygame.mixer.music.set_volume(slider.volume)
        pygame.time.delay(15)
        pygame.display.update()


def write_score(score):
    with open("high_score.txt", "r") as f:
        arr = [int(i) for i in f.readlines()]
    arr.append(score)
    arr.sort(reverse=True)

    with open("high_score.txt", "w") as f:
        for i in range(3):
            f.write(str(arr[i])+'\n')


def show_score(screen):
    boo = True
    time = 0
    back = Button('Back', (0, 0, 0), (255, 255, 0), big_font)
    black = True
    with open("high_score.txt", "r") as f:
        text_arr = f.read().split('\n')
        w = 0
        h = 0
    arr = []
    for i in range(3):
        arr.append(big_font.render(text_arr[i], False, (0, 0, 0)))
        w = arr[i].get_width() if arr[i].get_width() > w else w
        h = arr[i].get_height()
    while boo:
        time += 15
        screen.fill(constants_and_variables.main_color)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    boo = False
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                if back.check_click():
                    boo = False
        if time >= 700 and black:
            time = 0
            black = False
            for i in range(3):
                arr[i] = big_font.render(text_arr[i], False, (255, 255, 0))
        elif time >= 700:
            time = 0
            black = True
            for i in range(3):
                arr[i] = big_font.render(text_arr[i], False, (0, 0, 0))

        for i in range(3):
            screen.blit(arr[i], (screen.get_width() // 2 - w // 2, screen.get_height() // 2 - 120 + (h * (i + 1) + 30 * i) // 2))
        back.show(screen, screen.get_width() // 2 - back.width // 2, 500)
        pygame.time.delay(15)
        pygame.display.update()
