import Ghost


class Score:
    def __init__(self, font):
        self.font = font

    def show(self, screen, x, y, color):
        surf = self.font.render('score: ' + str(Ghost.Ghost.count), False, color)
        screen.blit(surf, (x, y))

