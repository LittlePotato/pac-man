import pygame


class Button:
    def __init__(self, text, color_off, color_on, font):
        self.text = text
        self.color_off = color_off
        self.color_on = color_on
        self.cur_color = self.color_off
        self.width = 0
        self.height = 0
        self.x = 0
        self.y = 0
        self.disabled = False
        self.disabled_color = ((self.color_off[0] + 255) // 2, (self.color_off[1] + 255) // 2,
                               (self.color_off[2] + 255) // 2)
        self.font = font
        self.set_width_height()

    def check_click(self):
        pos = pygame.mouse.get_pos()
        if self.x - 5 <= pos[0] <= self.x + 5 + self.width and self.y - 5 <= pos[1] <= self.y + 5 + self.height and not self.disabled:
            return True
        else:
            return False

    def set_width_height(self):
        textsurface = self.font.render(self.text, False, self.color_off)
        self.width = textsurface.get_width()
        self.height = textsurface.get_height()

    def show(self, screen, x, y):     # x and y is left top angle
        self.check_mouse()
        textsurface = self.font.render(self.text, False, self.cur_color) if not self.disabled\
            else self.font.render(self.text, False, self.disabled_color)
        self.width = textsurface.get_width()
        self.height = textsurface.get_height()
        self.x = x
        self.y = y
        screen.blit(textsurface, (self.x, self.y))
        if self.cur_color == self.color_on and not self.disabled:
            pygame.draw.circle(screen, self.color_on, (self.x - 5 - self.height // 4, self.y + self.height // 2), self.height // 6, 0)

    def check_mouse(self):
        pos = pygame.mouse.get_pos()
        if self.x - 5 <= pos[0] <= self.x + 5 + self.width and self.y - 5 <= pos[1] <= self.y + + 5 + self.height and not self.disabled:
            self.cur_color = self.color_on
        else:
            self.cur_color = self.color_off
