import Ghost
import pygame


class Pinky(Ghost.Ghost):
    def __init__(self, size, speed, delay, x, y):
        Ghost.Ghost.__init__(self, size, speed, delay, x, y)
        self.direction = 'a'
        self.image = ['Ghosts-sprites/Pinky/Pinky_up_0.png', 'Ghosts-sprites/Pinky/Pinky_up_1.png',
                      'Ghosts-sprites/Pinky/Pinky_left_0.png', 'Ghosts-sprites/Pinky/Pinky_left_1.png',
                      'Ghosts-sprites/Pinky/Pinky_down_0.png', 'Ghosts-sprites/Pinky/Pinky_down_1.png',
                      'Ghosts-sprites/Pinky/Pinky_right_0.png', 'Ghosts-sprites/Pinky/Pinky_right_1.png']
        for i in range(8):
            self.image[i] = pygame.transform.scale(pygame.image.load(self.image[i]), (self.size, self.size)).convert()
            self.image[i].set_colorkey((0, 0, 0))

    def move_pinky(self, field, pacman, blinky, inky, clyde, screen, score, heart, high_score):

        # ------------target------------------
        if self.alive and self.out:
            if Ghost.Ghost.mode == 'chase':
                if pacman.direction == 'a':
                    self.target = (pacman.x - 16 * 2, pacman.y)
                elif pacman.direction == 'd':
                    self.target = (pacman.x + 16 * 2, pacman.y)
                elif pacman.direction == 'w':
                    self.target = (pacman.x, pacman.y - 16 * 2)
                elif pacman.direction == 's':
                    self.target = (pacman.x, pacman.y + 16 * 2)
            elif Ghost.Ghost.mode == 'scatter':
                self.target = (0, 0)

        self.check_pacman_collision(pacman, [blinky, self, inky, clyde], screen, field, score, heart, high_score)

        Ghost.Ghost.move(self, screen, field)
