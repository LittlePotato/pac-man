import random
import sys
import pygame
from constants_and_variables import volume, music_arr, bool_for_game
from game_func import pause, settings, show_score
import Ghost


def handle_menu_events(screen, start, sett, score, ex):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            if start.check_click():
                bool_for_game['menu'] = False
                bool_for_game['game'] = True
            if sett.check_click():
                settings(screen)
            if score.check_click():
                pass
            if ex.check_click():
                pygame.quit()
                sys.exit()
            if score.check_click():
                show_score(screen)

        elif event.type == pygame.mixer.music.get_endevent():
            pygame.mixer.music.load('music/' + random.choice(music_arr))
            pygame.mixer.music.play()
            pygame.mixer.music.set_volume(volume)


def handle_events_for_game(screen, field, pacman, score, ghosts):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            pos = pygame.mouse.get_pos()
            y = pos[0] // field.arr[0][0].width
            x = pos[1] // field.arr[0][0].width
            print(field.arr[x][y].ways)
            print(field.arr[x][y].coo)
            print(field.arr[x][y].state)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE or event.key == pygame.K_p:
                Ghost.pause_time = pause(screen, score)
        elif event.type == pygame.mixer.music.get_endevent():
            pygame.mixer.music.load('music/' + random.choice(music_arr))
            pygame.mixer.music.play()
            pygame.mixer.music.set_volume(volume)

    keys = pygame.key.get_pressed()

    cache = None
    if keys[pygame.K_UP]:
        cache = 'w'
    elif keys[pygame.K_DOWN]:
        cache = 's'
    elif keys[pygame.K_RIGHT]:
        cache = 'd'
    elif keys[pygame.K_LEFT]:
        cache = 'a'
    if cache is not None:
        pacman.cache = cache
        pacman.cache_timer = 2 * (field.arr[0][0].width // pacman.speed) + 1

    n = None
    if keys[pygame.K_0]:
        n = 0
    elif keys[pygame.K_1]:
        n = 1
    elif keys[pygame.K_2]:
        n = 2
    elif keys[pygame.K_3]:
        n = 3
    elif keys[pygame.K_4]:
        n = 4
    if n is not None:
        for i in range(4):
            if i == n - 1 and ghosts[i].out and ghosts[i].alive:
                ghosts[i].auto = False
            else:
                ghosts[i].auto = True
            ghosts[i].cache = None

    cache_ghost = None
    if keys[pygame.K_s]:
        cache_ghost = 's'
    elif keys[pygame.K_w]:
        cache_ghost = 'w'
    elif keys[pygame.K_a]:
        cache_ghost = 'a'
    elif keys[pygame.K_d]:
        cache_ghost = 'd'

    for ghost in ghosts:
        if not ghost.auto:
            if cache_ghost is not None:
                ghost.cache = cache_ghost
                ghost.cache_timer = 2 * (field.arr[0][0].width // ghost.speed) + 1


def handle_over_events(restart, menu):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            if restart.check_click():
                bool_for_game['menu'] = False
                bool_for_game['game'] = True
                bool_for_game['over'] = False
                bool_for_game['win'] = False
            if menu.check_click():
                bool_for_game['menu'] = True
                bool_for_game['game'] = False
                bool_for_game['over'] = False
                bool_for_game['win'] = False
        elif event.type == pygame.mixer.music.get_endevent():
            pygame.mixer.music.load('music/' + random.choice(music_arr))
            pygame.mixer.music.play()
            pygame.mixer.music.set_volume(volume)
