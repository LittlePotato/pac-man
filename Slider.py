import pygame

icon_no = pygame.image.load('icons/no_music.png')
icon_min = pygame.image.load('icons/min_music.png')
icon_middle = pygame.image.load('icons/middle_music.png')
icon_max = pygame.image.load('icons/max_music.png')
icon_arr = [icon_no, icon_min, icon_middle, icon_max]


class Slider:
    def __init__(self, volume, delay, font=None, line_wid=7, icon_size=(30, 20), color=(99, 101, 112), leng=200, width_of_circle=21, slider_image=None,
                  sound_image_arr=None, animation=False, orientation='horizontal'):
        self.orientation = orientation        # horizontal, vertical
        self.x, self.y = 0, 0
        self.animation = animation
        self.len = leng
        self.width_of_circle = width_of_circle
        self.color = color
        self.icon_size = icon_size
        self.line_wid = line_wid
        self.space = width_of_circle
        self.down_mouse = False
        self.position = int(volume * self.len)
        self.circle_coo = None
        self.font = font
        self.delay = delay
        self.color_right = (209, 213, 215)
        self.volume = volume
        self.timer = 0
        self.sound_image_arr = sound_image_arr if sound_image_arr is not None else icon_arr
        if sound_image_arr is None:
            self.sound_image_arr = icon_arr if self.animation else [icon_arr[1], icon_arr[3]]
        self.slider_image = slider_image

        if self.slider_image is not None:
            self.slider_image = pygame.transform.scale(self.slider_image, (self.width_of_circle, self.width_of_circle))

        for i in range(len(self.sound_image_arr)):
            self.sound_image_arr[i] = pygame.transform.scale(self.sound_image_arr[i], (self.icon_size[0], self.icon_size[1]))

        self.full_width, self.full_height = self.set_w_h()

    def set_w_h(self):
        if self.orientation == 'horizontal':
            if self.animation:
                w = self.icon_size[0] + self.space + self.len
            else:
                w = 2 * self.icon_size[0] + 2 * self.space + self.len
            h = self.icon_size[1] if self.icon_size[1] > self.width_of_circle else self.width_of_circle
        else:
            if self.animation:
                h = self.icon_size[1] + self.space + self.len
            else:
                h = 2 * self.icon_size[1] + 2 * self.space + self.len
            w = self.icon_size[0] if self.icon_size[0] > self.width_of_circle else self.width_of_circle
        return w, h

    def animation_show(self, screen):
        pass

    def static_show(self, screen):
        if self.orientation == 'horizontal':
            x = self.x
            screen.blit(self.sound_image_arr[0], (x, self.y + self.full_height // 2 - self.icon_size[1] // 2))
            x += self.icon_size[0] + self.space
            pygame.draw.line(screen, self.color, (x, self.y + self.full_height // 2 - self.line_wid // 2),
                             (x + self.position, self.y + self.full_height // 2 - self.line_wid // 2),
                             self.line_wid)
            x += self.position
            pygame.draw.line(screen, self.color_right, (x, self.y + self.full_height // 2 - self.line_wid // 2),
                             (x + self.len - self.position,
                              self.y + self.full_height // 2 - self.line_wid // 2), self.line_wid)
            self.circle_coo = [x, self.y + self.full_height // 2] if self.circle_coo is None else self.circle_coo
            pygame.draw.circle(screen, self.color, self.circle_coo, self.width_of_circle // 2, 2)
            pygame.draw.circle(screen, (255, 255, 255), self.circle_coo, self.width_of_circle // 2, 0)
            pygame.draw.circle(screen, self.color, self.circle_coo, self.width_of_circle // 4, 0)
            x += self.len - self.position - self.width_of_circle // 2 + self.space
            screen.blit(self.sound_image_arr[1], (x, self.y + self.full_height // 2 - self.icon_size[1] // 2))
            if (self.down_mouse or self.timer > 0) and self.font is not None:
                t = self.font.render(str(int(self.volume * 100)) + '%', False, self.color)
                if self.timer > 0 and not self.down_mouse:
                    self.timer -= self.delay / 1000
                screen.blit(t, (self.circle_coo[0] - t.get_width() // 2, self.circle_coo[1] - self.width_of_circle // 2 - 5 - t.get_height()))
        else:
            y = self.y
            screen.blit(self.sound_image_arr[0], (self.x + self.full_width // 2 - self.icon_size[0] // 2, y))
            y += self.icon_size[1] + self.space
            pygame.draw.line(screen, self.color, (self.x + self.full_width // 2 - self.line_wid // 2, y),
                             (self.x + self.full_width // 2 - self.line_wid // 2,
                              y + self.position), self.line_wid)
            y += self.position
            pygame.draw.line(screen, self.color_right, (self.x + self.full_width // 2 - self.line_wid // 2, y),
                             (self.x + self.full_width // 2 - self.line_wid // 2,
                              y + self.len - self.position), self.line_wid)
            self.circle_coo = [self.x + self.full_width // 2, y] if self.circle_coo is None else self.circle_coo
            pygame.draw.circle(screen, self.color, self.circle_coo,
                               self.width_of_circle // 2, 2)
            pygame.draw.circle(screen, (255, 255, 255), self.circle_coo,
                               self.width_of_circle // 2 - 2, 0)
            pygame.draw.circle(screen, self.color, self.circle_coo,
                               self.width_of_circle // 4, 0)
            y += self.len - self.position - self.width_of_circle // 2 + self.space
            screen.blit(self.sound_image_arr[1], (self.x + self.full_width // 2 - self.icon_size[0] // 2, y))
            if (self.down_mouse or self.timer > 0) and self.font is not None:
                if self.timer > 0 and not self.down_mouse:
                    self.timer -= self.delay / 1000
                t = self.font.render(str(int(self.volume * 100)) + '%', False, self.color)
                screen.blit(t, (self.circle_coo[0] + self.width_of_circle // 2 + 5, self.circle_coo[1] - t.get_height() // 2))

    def show(self, screen, x, y):
        self.x = x
        self.y = y
        if self.animation:
            self.animation_show(screen)
        else:
            self.static_show(screen)

    def check_mouse_move(self):
        if self.down_mouse:
            pos = pygame.mouse.get_pos()
            if self.orientation == 'horizontal' and self.x + self.icon_size[0] + self.space <= pos[0] \
                    <= self.x + self.icon_size[0] + self.space + self.len:
                self.position += pos[0] - self.circle_coo[0]
                self.circle_coo[0] = pos[0]
            elif self.orientation == 'horizontal' and self.x + self.icon_size[0] + self.space >= pos[0]:
                self.position = 0
                self.circle_coo[0] = self.x + self.icon_size[0] + self.space
            elif self.orientation == 'horizontal':
                self.position = self.len
                self.circle_coo[0] = self.x + self.icon_size[0] + self.space + self.len
            if self.orientation == 'vertical' and self.y + self.icon_size[1] + self.space <= pos[1] <= self.y + \
                    self.icon_size[1] + self.space + self.len:
                self.position += pos[1] - self.circle_coo[1]
                self.circle_coo[1] = pos[1]
            elif self.orientation == 'vertical' and self.y + self.icon_size[1] + self.space >= pos[1]:
                self.position = 0
                self.circle_coo[1] = self.y + self.icon_size[1] + self.space
            elif self.orientation == 'vertical':
                self.position = self.len
                self.circle_coo[1] = self.y + self.icon_size[1] + self.space + self.len
            self.volume = self.position / self.len

    def check_mouse_icon(self):
        if not self.animation:
            return

    def check_mouse_click(self):
        pos = pygame.mouse.get_pos()
        if self.circle_coo[0] - self.width_of_circle // 2 <= pos[0] <= self.circle_coo[0] + self.width_of_circle // 2 \
                and self.circle_coo[1] - self.width_of_circle // 2 <= pos[1] <= self.circle_coo[1] + self.width_of_circle // 2:
            self.down_mouse = True
            self.timer = 0.5
        elif self.orientation == 'horizontal' and self.y + self.full_height // 2 - self.line_wid // 2 <= pos[1] <=\
                self.y + self.full_height // 2 + self.line_wid // 2 and self.x + self.icon_size[0] + self.space <=\
                pos[0] <= self.x + self.icon_size[0] + self.space + self.len:
            self.position += pos[0] - self.circle_coo[0]
            self.volume = self.position / self.len
            self.circle_coo[0] = pos[0]
            self.down_mouse = True
            self.timer = 0.5
        elif self.orientation == 'vertical' and self.x + self.full_width // 2 - self.line_wid // 2 <= pos[0] <=\
                self.x + self.full_width // 2 + self.line_wid // 2 and self.y + self.icon_size[1] + self.space <=\
                pos[1] <= self.y + self.icon_size[1] + self.space + self.len:
            self.position += pos[1] - self.circle_coo[1]
            self.volume = self.position / self.len
            self.circle_coo[1] = pos[1]
            self.down_mouse = True
            self.timer = 0.5

    def unclick(self):        # have to be called in MOUSEBUTTONUP event
        self.down_mouse = False
