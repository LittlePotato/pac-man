from math import sqrt
from random import choice
import pygame
import game_func
from Character import Character


class Ghost(Character):
    mode = 'chase'   # chase scatter frightened
    count = 0
    fri_timer = 0
    timer = 0
    dict = {'left': 'a', 'right': 'd', 'up': 'w', 'down': 's'}

    def __init__(self, size, speed, delay, x, y):
        Character.__init__(self, size, speed, delay, x, y)
        self.auto = True
        self.in_center = False
        self.out = False
        self.alive = True
        self.target = (0, 0)
        self.direction = 'a'
        self.image_eyes = ['Ghosts-sprites/eyes0.png', 'Ghosts-sprites/eyes1.png', 'Ghosts-sprites/eyes2.png',
                           'Ghosts-sprites/eyes3.png']
        self.image_fear = ['Ghosts-sprites/fear0.png', 'Ghosts-sprites/fear1.png', 'Ghosts-sprites/fear2.png',
                           'Ghosts-sprites/fear3.png']

        for i in range(4):
            self.image_eyes[i] = pygame.transform.scale(pygame.image.load(self.image_eyes[i]), (self.size, self.size // 3)).convert()
            self.image_fear[i] = pygame.transform.scale(pygame.image.load(self.image_fear[i]), (self.size, self.size)).convert()
            self.image_eyes[i].set_colorkey((0, 0, 0))
            self.image_fear[i].set_colorkey((0, 0, 0))

        self.image = []

    def draw(self, screen):
        self.time += self.delay
        if self.time >= 100:
            self.time = 0
            self.cur = (self.cur + 1) % 2

        if self.alive and Ghost.mode != 'frightened':
            self.speed = self.default_speed
            if self.direction == 'd':
                screen.blit(self.image[self.cur + 6], (self.x - 8, self.y - 8))
            elif self.direction == 'a':
                screen.blit(self.image[self.cur + 2], (self.x - 8, self.y - 8))
            elif self.direction == 'w':
                screen.blit(self.image[self.cur], (self.x - 8, self.y - 8))
            else:
                screen.blit(self.image[self.cur + 4], (self.x - 8, self.y - 8))

        elif not self.alive:
            if self.direction == 'd':
                screen.blit(self.image_eyes[0], (self.x - self.size // 2, self.y - self.size // 6))
            elif self.direction == 'a':
                screen.blit(self.image_eyes[1], (self.x - self.size // 2, self.y - self.size // 6))
            elif self.direction == 'w':
                screen.blit(self.image_eyes[2], (self.x - self.size // 2, self.y - self.size // 6))
            else:
                screen.blit(self.image_eyes[3], (self.x - self.size // 2, self.y - self.size // 6))

        elif Ghost.mode == 'frightened':
            self.speed = self.default_speed * 0.7
            if Ghost.fri_timer <= 5000:
                screen.blit(self.image_fear[self.cur], (self.x - 8, self.y - 8))
            elif 7000 >= Ghost.fri_timer >= 5000:
                screen.blit(self.image_fear[self.cur + 1], (self.x - 8, self.y - 8))
            else:
                Ghost.mode = 'chase'

    def check_pacman_collision(self, pacman, ghosts, screen, field, score, heart, high_score):
        row_cell, column_cell = self.y // field.arr[0][0].width, self.x // field.arr[0][0].width
        row_cell_p, column_cell_p = pacman.y // field.arr[0][0].width, pacman.x // field.arr[0][0].width
        if row_cell == row_cell_p and column_cell == column_cell_p and Ghost.mode == 'frightened' and self.alive:
            self.alive = False
            self.speed = self.default_speed * 1.5
            if not self.auto:
                self.auto = True
            if pacman.kill_ghosts == 0:
                Ghost.count += 200
            elif pacman.kill_ghosts == 1:
                Ghost.count += 400
            elif pacman.kill_ghosts == 2:
                Ghost.count += 800
            else:
                Ghost.count += 1600
            pacman.kill_ghosts += 1
            pacman.all_kills += 1
            return
        elif row_cell == row_cell_p and column_cell == column_cell_p and pacman.life > 1 and self.alive:
            if not self.auto:
                self.auto = True
            game_func.show_pacman_death(screen, field, ghosts[0], ghosts[1], ghosts[2], ghosts[3], pacman, score)
            pacman.life -= 1
            game_func.show_start(1800, screen, field, ghosts[0], ghosts[1], ghosts[2], ghosts[3], pacman, score, heart, high_score)
        elif row_cell == row_cell_p and column_cell == column_cell_p and self.alive:
            game_func.show_pacman_death(screen, field, ghosts[0], ghosts[1], ghosts[2], ghosts[3], pacman, score)
            pacman.death = True

    def move(self, screen, field, ghosts=None):
        if not self.auto:
            pygame.draw.circle(screen, (255, 255, 0), (int(self.x), int(self.y)), self.size // 2 + 5, 1)
            Character.move(self, screen, field, ghosts)
            return

        row_cell, column_cell = int(self.y // field.arr[0][0].width), int(self.x // field.arr[0][0].width)

        # -----------------tunnel---------------
        if self.direction == 'a' and (self.x - self.speed) // field.arr[0][0].width < 0:
            self.x = field.arr[0][27].coo[0] + field.arr[0][0].width
            column_cell = 27
        elif self.direction == 'd' and (self.x + self.speed) // field.arr[0][0].width > 27:
            self.x = field.arr[0][0].coo[0]
            column_cell = 0

        if self.direction == 'd':
            self.x += self.speed
        elif self.direction == 'a':
            self.x -= self.speed
        elif self.direction == 'w':
            self.y -= self.speed
        elif self.direction == 's':
            self.y += self.speed

        # ---------------put the ghost in the center of a sell-----------------
        if self.direction == 'd' and self.x < field.arr[row_cell][column_cell].coo[0] + field.arr[row_cell][column_cell].width // 2\
                <= self.x + self.speed:
            self.x = field.arr[row_cell][column_cell].coo[0] + field.arr[row_cell][column_cell].width // 2

        elif self.direction == 'a' and self.x > field.arr[row_cell][column_cell].coo[0] + field.arr[row_cell][column_cell].width // 2\
                >= self.x - self.speed:
            self.x = field.arr[row_cell][column_cell].coo[0] + field.arr[row_cell][column_cell].width // 2

        elif self.direction == 'w' and self.y > field.arr[row_cell][column_cell].coo[1] + field.arr[row_cell][column_cell].width // 2 \
                >= self.y - self.speed:
            self.y = field.arr[row_cell][column_cell].coo[1] + field.arr[row_cell][column_cell].width // 2

        elif self.direction == 's' and self.y < field.arr[row_cell][column_cell].coo[1] + field.arr[row_cell][column_cell].width // 2 \
                <= self.y + self.speed:
            self.y = field.arr[row_cell][column_cell].coo[1] + field.arr[row_cell][column_cell].width // 2

        elif self.direction != '':
            return

        # ------------dead-----------------
        if not self.alive:
            if self.out:
                self.target = (field.door[0] + 8, field.door[1] - 8)  # above home
                if self.x == self.target[0] and self.y == self.target[1]:   # through the wall
                    self.out = False
                    self.direction = 's'
                    self.target = (field.door[0] + 8, field.door[1] + 40)
                    self.y += self.speed
                    return
            elif self.x == self.target[0] and self.y == self.target[1]:
                self.alive = True
                self.speed = self.default_speed
                self.direction = 'w'
                self.y -= self.speed
                self.target = (field.door[0] + 8, field.door[1] - 8)
                return

        # ------------go out-----------------
        elif not self.out:
            if not self.in_center:
                self.target = (field.door[0] + 8, field.door[1] + 40)
                if self.x == self.target[0] and self.y == self.target[1]:
                    self.in_center = True
                    self.direction = 'w'
                    self.target = (field.door[0] + 8, field.door[1] - 8)
            elif self.x == self.target[0] and self.y == self.target[1] + 16 * 2:  # through the wall
                self.direction = 'w'
                self.y -= self.speed
                return
            elif self.x == self.target[0] and self.y == self.target[1]:
                self.out = True
                self.direction = 'a'
                self.x -= self.speed
                return

        # --------------turn--------------------
        if len(field.arr[row_cell][column_cell].ways) >= 2:
            if Ghost.mode == 'frightened' and self.alive and self.out:
                while 1 == 1:
                    rand_dir = choice(field.arr[row_cell][column_cell].ways)
                    if rand_dir == 'left' and self.direction != 'd':
                        self.direction = Ghost.dict[rand_dir]
                        return

                    if rand_dir == 'right' and self.direction != 'a':
                        self.direction = Ghost.dict[rand_dir]
                        return

                    if rand_dir == 'down' and self.direction != 'w':
                        self.direction = Ghost.dict[rand_dir]
                        return

                    if rand_dir == 'up' and self.direction != 's':
                        self.direction = Ghost.dict[rand_dir]
                        return

            else:
                leng = 730
                new_dir = ''

                for i in field.arr[row_cell][column_cell].ways:

                    if i == 'up' and (self.direction != 's' or self.direction == ''):
                        l = sqrt((self.target[0] - field.arr[row_cell - 1][column_cell].coo[0]) ** 2 +
                                 (self.target[1] - field.arr[row_cell - 1][column_cell].coo[1]) ** 2)
                        if l < leng:
                            leng = l
                            new_dir = 'w'
                    if i == 'left' and (self.direction != 'd' or self.direction == ''):
                        l = sqrt((self.target[0] - field.arr[row_cell][column_cell - 1].coo[0]) ** 2 +
                                 (self.target[1] - field.arr[row_cell][column_cell - 1].coo[1]) ** 2)
                        if l < leng:
                            leng = l
                            new_dir = 'a'
                    if i == 'down' and (self.direction != 'w' or self.direction == ''):
                        l = sqrt((self.target[0] - field.arr[row_cell + 1][column_cell].coo[0]) ** 2 +
                                 (self.target[1] - field.arr[row_cell + 1][column_cell].coo[1]) ** 2)
                        if l < leng:
                            leng = l
                            new_dir = 's'
                    if i == 'right' and (self.direction != 'a' or self.direction == ''):
                        l = sqrt((self.target[0] - field.arr[row_cell][column_cell + 1].coo[0]) ** 2 +
                                 (self.target[1] - field.arr[row_cell][column_cell + 1].coo[1]) ** 2)
                        if l < leng:
                            leng = l
                            new_dir = 'd'
                self.direction = new_dir


