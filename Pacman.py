import pygame
import Ghost
from Character import Character
import Field


class Pacman(Character):
    def __init__(self, size, speed, delay, x, y):    # x, y - center
        Character.__init__(self, size, speed, delay, x, y)
        self.life = 3
        self.death = False
        self.kill_ghosts = 0
        self.all_kills = 0
        self.direction = 'a'
        self.image_left = [pygame.image.load('pac-man-sprites/Pac_0.png'),
                           pygame.image.load('pac-man-sprites/Pac_left_1.png'),
                           pygame.image.load('pac-man-sprites/Pac_left_2.png')]
        self.image_right = [pygame.image.load('pac-man-sprites/Pac_0.png'),
                           pygame.image.load('pac-man-sprites/Pac_right_1.png'),
                           pygame.image.load('pac-man-sprites/Pac_right_2.png')]
        self.image_up = [pygame.image.load('pac-man-sprites/Pac_0.png'),
                           pygame.image.load('pac-man-sprites/Pac_up_1.png'),
                           pygame.image.load('pac-man-sprites/Pac_up_2.png')]
        self.image_down = [pygame.image.load('pac-man-sprites/Pac_0.png'),
                           pygame.image.load('pac-man-sprites/Pac_down_1.png'),
                           pygame.image.load('pac-man-sprites/Pac_down_2.png')]
        for i in range(3):
            self.image_left[i] = pygame.transform.scale(self.image_left[i], (self.size, self.size)).convert()
            self.image_right[i] = pygame.transform.scale(self.image_right[i], (self.size, self.size)).convert()
            self.image_up[i] = pygame.transform.scale(self.image_up[i], (self.size, self.size)).convert()
            self.image_down[i] = pygame.transform.scale(self.image_down[i], (self.size, self.size)).convert()

            self.image_left[i].set_colorkey((0, 0, 0))
            self.image_right[i].set_colorkey((0, 0, 0))
            self.image_up[i].set_colorkey((0, 0, 0))
            self.image_down[i].set_colorkey((0, 0, 0))

    @staticmethod
    def change_ghosts_directions(ghosts):
        for ghost in ghosts:
            if ghost.direction == 'd' and ghost.out and ghost.alive and ghost.auto:
                ghost.direction = 'a'
            elif ghost.direction == 'a' and ghost.out and ghost.alive and ghost.auto:
                ghost.direction = 'd'
            elif ghost.direction == 'w' and ghost.out and ghost.alive and ghost.auto:
                ghost.direction = 's'
            elif ghost.direction == 's' and ghost.out and ghost.alive and ghost.auto:
                ghost.direction = 'w'

    def draw(self, screen):
        self.time += self.delay
        if self.time >= 100:
            self.time = 0
            self.cur = (self.cur + 1) % 3

        if self.direction == '':
            screen.blit(self.image_down[0], (self.x - self.size // 2, self.y - self.size // 2))
        elif self.direction == 'a':
            screen.blit(self.image_left[self.cur], (self.x - self.size // 2, self.y - self.size // 2))
        elif self.direction == 'd':
            screen.blit(self.image_right[self.cur], (self.x - self.size // 2, self.y - self.size // 2))
        elif self.direction == 'w':
            screen.blit(self.image_up[self.cur], (self.x - self.size // 2, self.y - self.size // 2))
        else:
            screen.blit(self.image_down[self.cur], (self.x - self.size // 2, self.y - self.size // 2))

    def move(self, field, ghosts, screen=None):
        # ---------Ghost part----------
        if Ghost.Ghost.mode == 'frightened':
            Ghost.Ghost.fri_timer += self.delay
        else:
            Ghost.Ghost.timer += self.delay
            if Ghost.Ghost.mode == 'chase' and Ghost.Ghost.timer >= 15000:
                Pacman.change_ghosts_directions(ghosts)
                Ghost.Ghost.timer = 0
                Ghost.Ghost.mode = 'scatter'
            elif Ghost.Ghost.mode == 'scatter' and Ghost.Ghost.timer >= 5000:
                Ghost.Ghost.timer = 0
                Ghost.Ghost.mode = 'chase'

        # ----------------------------------

        Character.move(self, screen, field, ghosts=None)
        row_cell, column_cell = self.y // field.arr[0][0].width, self.x // field.arr[0][0].width

        # -----------------grains---------------
        if field.arr[row_cell][column_cell].state == 2:
            field.arr[row_cell][column_cell].state = 0
            Ghost.Ghost.count += 10
            Field.Field.grains -= 1
        elif field.arr[row_cell][column_cell].state == 3:
            field.arr[row_cell][column_cell].state = 0
            self.kill_ghosts = 0
            Ghost.Ghost.count += 50
            Ghost.Ghost.mode = 'frightened'
            Pacman.change_ghosts_directions(ghosts)
            Ghost.Ghost.fri_timer = 0
            Field.Field.grains -= 1
