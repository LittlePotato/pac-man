import Cell
import constants_and_variables


class Field:
    map = constants_and_variables.map
    grains = 0

    def __init__(self, screen_width):
        Field.map = constants_and_variables.map
        self.width = screen_width
        self.width_of_one_sell = screen_width // 28
        self.x, self.y = 0, self.width_of_one_sell * 3
        self.door = (0, 0)
        self.arr = self.make_sq_arr()

    def make_sq_arr(self):
        arr = []
        with open(Field.map + '.txt', 'r') as f:
            for i in range(36):
                arr.append([])
                for j in range(28):
                    x = f.read(1)
                    if x == '\n':
                        x = f.read(1)

                    arr[i].append(Cell.Cell(int(x), self.width_of_one_sell, [j * self.width_of_one_sell, i * self.width_of_one_sell]))
                    if arr[i][j].state == 4:
                        arr[i][j].state = 1
                        arr[i][j].double_wall = True
                    elif arr[i][j].state == 5:
                        arr[i][j].state = 1
                        arr[i][j].door = True
                        if self.door[0] == 0 and self.door[1] == 0:
                            self.door = (j * self.width_of_one_sell, i * self.width_of_one_sell)
                    if int(x) == 2 or int(x) == 3:
                        Field.grains += 1

        for i in range(36):
            for j in range(28):
                if arr[i][j].state != 1:
                    if j >= 1:
                        if arr[i][j - 1].state != 1:
                            arr[i][j].ways.append('left')
                    if j <= 26:
                        if arr[i][j + 1].state != 1:
                            arr[i][j].ways.append('right')
                    if i >= 1:
                        if arr[i - 1][j].state != 1:
                            arr[i][j].ways.append('up')
                    if i <= 34:
                        if arr[i + 1][j].state != 1:
                            arr[i][j].ways.append('down')

        return arr

    def draw(self, screen):
        screen.fill((0, 0, 0))
        for i in range(36):
            for j in range(28):
                self.arr[i][j].draw(screen, self, i, j)
